import React from 'react';
import { View } from 'react-native';
import NavTabs from 'src/components/navigation/nav-tabs';

const Default = () => (
  <View>
    <NavTabs />
  </View>
);

export default Default;
