import React from 'react';
import PropTypes from 'prop-types';
import { View, Text } from 'react-native';

const Default = ({ title }) => (
  <View>
    <Text>{title}</Text>
  </View>
);

Default.propTypes = {
  title: PropTypes.string
};

Default.defaultProps = {
  title: 'defaultTitle'
};

export default Default;
