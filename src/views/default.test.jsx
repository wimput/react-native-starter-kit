import React from 'react';
import { shallow } from 'enzyme';
import Default from 'src/views/default';

const wrapper = shallow(<Default />);

describe('combined-providers', () => {
  it('should render correctly', () => {
    expect(wrapper).toBeTruthy();
  });
});
