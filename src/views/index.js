export { default as defaultView } from 'src/views/default';
export { default as defaultViewWithTabs } from 'src/views/defaultWithTabs';
