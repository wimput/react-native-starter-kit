import routes from 'src/contexts/routes';

describe('members', () => {
  it('should expose a Provider', () => {
    expect(routes.Provider).toBeTruthy();
  });

  it('should expose a Consumer', () => {
    expect(routes.Consumer).toBeTruthy();
  });
});

