import views from 'src/contexts/views';

describe('members', () => {
  it('should expose a Provider', () => {
    expect(views.Provider).toBeTruthy();
  });

  it('should expose a Consumer', () => {
    expect(views.Consumer).toBeTruthy();
  });
});
