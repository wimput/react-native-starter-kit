import React from 'react';
import { Provider } from 'react-redux';
import { StyleProvider } from 'native-base';
import PropTypes from 'prop-types';
import store from 'src/store';
import routes from 'src/routes';
import ViewContext from 'src/contexts/views';
import RouteContext from 'src/contexts/routes';
import getTheme from 'native-base-theme/components';
import platform from 'native-base-theme/variables/platform';
import * as allViews from 'src/views';

const CombinedProviders = props => (
  <StyleProvider style={getTheme(platform)}>
    <Provider store={store}>
      <ViewContext.Provider value={allViews}>
        <RouteContext.Provider value={routes}>
          {props.children}
        </RouteContext.Provider>
      </ViewContext.Provider>
    </Provider>
  </StyleProvider>
);

CombinedProviders.propTypes = {
  children: PropTypes.node.isRequired
};

export default CombinedProviders;
