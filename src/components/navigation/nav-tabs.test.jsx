import React from 'react';
import { shallow } from 'enzyme';
import { Tab } from 'native-base';
import RouteContext from 'src/contexts/routes';
import { NavTabsBase } from 'src/components/navigation/nav-tabs';

const Component1 = jest.fn();
const Component2 = jest.fn();
const Component3 = jest.fn();

const routes = [
  {
    id: 'testPage1',
    label: 'TestPage1',
    icon: 'person',
    view: {
      component: Component1
    },
    routes: 'subRoutes1'
  },
  {
    id: 'testPage2',
    label: 'TestPage2',
    view: {
      component: Component2
    }
  },
  {
    id: 'testPage3',
    label: 'TestPage3',
    icon: 'market',
    view: {
      component: Component3
    },
    routes: 'subRoutes3'
  }
];

const wrapper = shallow(<NavTabsBase routes={routes} />);

const tabs = wrapper.find(Tab);

describe('NavTabs', () => {
  it('should distribute routes over Tabs', () => {
    expect(wrapper.find(Tab).length).toEqual(3);
  });

  it('should render the route components', () => {
    expect(tabs.at(0).find(Component1).length).toEqual(1);
    expect(tabs.at(1).find(Component2).length).toEqual(1);
    expect(tabs.at(2).find(Component3).length).toEqual(1);
  });

  it('should wrap the route components with a new route provider', () => {
    expect(tabs.at(0).props().children.type).toEqual(RouteContext.Provider);
    expect(tabs.at(0).props().children.props.value).toEqual('subRoutes1');
    expect(tabs.at(1).props().children.type).toEqual(RouteContext.Provider);
    expect(tabs.at(1).props().children.props.value).toEqual([]);
    expect(tabs.at(2).props().children.type).toEqual(RouteContext.Provider);
    expect(tabs.at(2).props().children.props.value).toEqual('subRoutes3');
  });

  it('should set the initial tab to the first route', () => {
    expect(wrapper.props().initialPage).toEqual(0);
  });
});
