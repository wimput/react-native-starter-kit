import React from 'react';
import { Route, Switch, Redirect } from 'react-router-native';
import PropTypes from 'prop-types';
import RouteContext from 'src/contexts/routes';
import withContextRouteComponents from 'src/hoc/withContextRouteComponents';

export const RouterViewBase = ({ routes }) => (
  <Switch>
    {routes.map(route => (
      <Route
        key={`RouterView${route.id}`}
        path={route.path}
        render={() => (
          <RouteContext.Provider value={route.routes || []}>
            <route.view.component />
          </RouteContext.Provider>
        )}
      />
    ))}
    {routes.length && <Redirect to={routes[0].path} />}
  </Switch>
);

RouterViewBase.propTypes = {
  routes: PropTypes.array.isRequired
};

export default withContextRouteComponents(RouterViewBase);
