import React from 'react';
import { Tabs, Tab } from 'native-base';
import PropTypes from 'prop-types';
import RouteContext from 'src/contexts/routes';
import withContextRouteComponents from 'src/hoc/withContextRouteComponents';

export const NavTabsBase = ({ routes }) => (
  <Tabs initialPage={0}>
    {routes.map(route => (
      <Tab key={`navtab${route.id}`} heading={route.label}>
        <RouteContext.Provider value={route.routes || []}>
          <route.view.component />
        </RouteContext.Provider>
      </Tab>
    ))}
  </Tabs>
);

NavTabsBase.propTypes = {
  routes: PropTypes.array.isRequired
};

export default withContextRouteComponents(NavTabsBase);
