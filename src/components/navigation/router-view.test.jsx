import React from 'react';
import { shallow } from 'enzyme';
import { Route, Redirect } from 'react-router-native';
import { RouterViewBase } from 'src/components/navigation/router-view';
import RouteContext from 'src/contexts/routes';

const Component1 = jest.fn();
const Component2 = jest.fn();
const Component3 = jest.fn();

const props = {
  routes: [
    {
      id: 'testPage1',
      path: 'testPage1',
      label: 'TestPage1',
      icon: 'person',
      view: {
        component: Component1
      },
      routes: 'subRoutes1'
    },
    {
      id: 'testPage2',
      path: 'testPage2',
      label: 'TestPage2',
      view: {
        component: Component2
      }
    },
    {
      id: 'testPage3',
      path: 'testPage3',
      label: 'TestPage3',
      icon: 'market',
      view: {
        component: Component3
      },
      routes: 'subRoutes3'
    }
  ]
};

const wrapper = shallow(<RouterViewBase {...props} />);

const routes = wrapper.find(Route);

describe('NavTabs', () => {
  it('should distribute routes', () => {
    expect(routes.length).toEqual(3);
  });

  it('should render the route components', () => {
    expect(routes.at(0).props().render().props.children.type).toEqual(Component1);
    expect(routes.at(1).props().render().props.children.type).toEqual(Component2);
    expect(routes.at(2).props().render().props.children.type).toEqual(Component3);
  });

  it('should wrap the route components with a new route provider', () => {
    expect(routes.at(0).props().render().type).toEqual(RouteContext.Provider);
    expect(routes.at(0).props().render().props.value).toEqual('subRoutes1');
    expect(routes.at(1).props().render().type).toEqual(RouteContext.Provider);
    expect(routes.at(1).props().render().props.value).toEqual([]);
    expect(routes.at(2).props().render().type).toEqual(RouteContext.Provider);
    expect(routes.at(2).props().render().props.value).toEqual('subRoutes3');
  });

  it('should redirect to the first route', () => {
    expect(wrapper.find(Redirect).length).toEqual(1);
    expect(wrapper.find(Redirect).props().to).toEqual('testPage1');
  });
});
