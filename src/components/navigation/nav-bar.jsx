import React from 'react';
import { Text, StyleSheet } from 'react-native';
import { Button, FooterTab, Icon } from 'native-base';
import PropTypes from 'prop-types';
import withNavigationRoutes from 'src/hoc/withNavigationRoutes';
import variable from 'native-base-theme/variables/platform';

const styles = StyleSheet.create({
  text: {
    color: variable.tabBarTextColor
  },
  textActive: {
    color: variable.tabBarActiveTextColor
  }
});

export const NavBarBase = ({ routes }) => (
  <FooterTab>
    {routes.map(route => (
      <Button
        active={route.isActive}
        key={`NavBar${route.id}`}
        onPress={route.navigateTo}
      >
        {route.icon && <Icon name={route.icon} />}
        <Text
          style={route.isActive ? styles.textActive : styles.text}
        >
          {route.label}
        </Text>
      </Button>
    ))}
  </FooterTab>
);

NavBarBase.propTypes = {
  routes: PropTypes.array.isRequired
};

export default withNavigationRoutes(NavBarBase);
