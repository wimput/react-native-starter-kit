import React from 'react';
import { shallow } from 'enzyme';
import { Button, Icon } from 'native-base';
import variable from 'native-base-theme/variables/platform';
import { NavBarBase } from 'src/components/navigation/nav-bar';

const navigateToTest1 = jest.fn();
const navigateToTest2 = jest.fn();
const navigateToTest3 = jest.fn();

const routes = [
  {
    id: 'testPage1',
    label: 'TestPage1',
    icon: 'person',
    isActive: false,
    navigateTo: navigateToTest1
  },
  {
    id: 'testPage2',
    label: 'TestPage2',
    isActive: true,
    navigateTo: navigateToTest2
  },
  {
    id: 'testPage3',
    label: 'TestPage3',
    icon: 'market',
    isActive: false,
    navigateTo: navigateToTest3
  }
];

const wrapper = shallow(<NavBarBase routes={routes} />);

describe('NavBar', () => {
  it('should distribute routes over Buttons', () => {
    expect(wrapper.find(Button).length).toEqual(3);
  });

  it('should set the active prop correctly for all Buttons', () => {
    const buttons = wrapper.find(Button);
    expect(buttons.get(0).props.active).toEqual(false);
    expect(buttons.get(1).props.active).toEqual(true);
    expect(buttons.get(2).props.active).toEqual(false);
  });

  it('should set the correct key for all Buttons', () => {
    const buttons = wrapper.find(Button);
    expect(buttons.at(0).key()).toEqual('NavBartestPage1');
    expect(buttons.at(1).key()).toEqual('NavBartestPage2');
    expect(buttons.at(2).key()).toEqual('NavBartestPage3');
  });

  it('should call the correct navigateTo method when interacting with a Button', () => {
    const buttons = wrapper.find(Button);
    buttons.get(1).props.onPress();
    expect(navigateToTest1).not.toHaveBeenCalled();
    expect(navigateToTest2).toHaveBeenCalled();
    expect(navigateToTest3).not.toHaveBeenCalled();
  });

  it('should display the correct Icons', () => {
    const icons = wrapper.find(Icon);
    expect(icons.length).toEqual(2);
    expect(icons.get(0).props.name).toEqual('person');
    expect(icons.get(1).props.name).toEqual('market');
  });

  it('should display the correct labels', () => {
    const texts = wrapper.find(Button).map(b => b.props().children[1].props.children);
    expect(texts[0]).toEqual('TestPage1');
    expect(texts[1]).toEqual('TestPage2');
    expect(texts[2]).toEqual('TestPage3');
  });

  it('should set the correct text colors', () => {
    const colors = wrapper.find(Button).map(b => b.props().children[1].props.style.color);
    expect(colors[0]).toEqual(variable.tabBarTextColor);
    expect(colors[1]).toEqual(variable.tabBarActiveTextColor);
    expect(colors[2]).toEqual(variable.tabBarTextColor);
  });
});
