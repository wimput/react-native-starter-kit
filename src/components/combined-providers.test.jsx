import React from 'react';
import { shallow } from 'enzyme';
import CombinedProviders from 'src/components/combined-providers';

jest.mock('src/views/index.js');
jest.mock('src/store/index.js');
jest.mock('src/store/reducers/index.js');
jest.mock('src/store/reducers/members');
jest.mock('native-base-theme/components/index.js');
jest.mock('native-base-theme/variables/platform.js');

const Dummy = () => {};

const wrapper = shallow(<CombinedProviders><Dummy /></CombinedProviders>);

describe('combined-providers', () => {
  it('should render correctly', () => {
    expect(wrapper).toBeTruthy();
  });
});

