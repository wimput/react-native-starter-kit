import React from 'react';
import { shallow } from 'enzyme';
import withRouteComponents from 'src/hoc/withRouteComponents';

const views = {
  dummy1: () => {},
  dummy2: () => {}
};

const routes = [
  { view: { type: 'dummy1' } },
  { view: { type: 'dummy2' } }
];

const Dummy = () => {};

const Test = withRouteComponents(Dummy);

const wrapper = shallow(<Test routes={routes} />);

describe('withRouteComponents', () => {
  it('should add actual components to routes', () => {
    const propRoutes = wrapper.props().children(views).props.routes;
    expect(propRoutes[0].view.component).toEqual(views.dummy1);
    expect(propRoutes[1].view.component).toEqual(views.dummy2);
  });
});
