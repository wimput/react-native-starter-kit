import { compose } from 'recompose';
import withContextRoutes from 'src/hoc/withContextRoutes';
import withRouteComponents from 'src/hoc/withRouteComponents';

export default compose(withContextRoutes, withRouteComponents);
