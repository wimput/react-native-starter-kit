import React from 'react';
import { shallow } from 'enzyme';
import withContextRoutes from 'src/hoc/withContextRoutes';

const Dummy = () => {};

const Test = withContextRoutes(Dummy);

const wrapper = shallow(<Test />);

describe('withContextRoutes', () => {
  it('should pass the route context as prop', () => {
    expect(wrapper.props().children('testValue').props.routes).toEqual('testValue');
  });
});
