import React from 'react';
import PropTypes from 'prop-types';

export default (Component) => {
  const withReactRouterBridge = props => (
    <Component
      {...props}
      routes={props.routes.map(route => ({
        ...route,
        isActive: props.location.pathname === route.path,
        navigateTo: () => props.history.push(route.path)
      }))}
    />
  );
  withReactRouterBridge.propTypes = {
    routes: PropTypes.array.isRequired,
    location: PropTypes.object.isRequired,
    history: PropTypes.object.isRequired
  };
  return withReactRouterBridge;
};
