import { withRouter } from 'react-router-native';
import withNavigationRoutes from 'src/hoc/withNavigationRoutes';
import withContextRoutes from 'src/hoc/withContextRoutes';
import withReactRouterBridge from 'src/hoc/withReactRouterBridge';

jest.mock('react-router-native', () => ({ withRouter: jest.fn().mockImplementation(comp => comp) }));
jest.mock('src/hoc/withContextRoutes', () => jest.fn().mockImplementation(comp => comp));
jest.mock('src/hoc/withReactRouterBridge', () => jest.fn().mockImplementation(comp => comp));

const Component = () => {};

describe('withNavigationRoutes', () => {
  it('should combine withRouter, withContextRoutes and withReactRouterBridge', () => {
    expect(withNavigationRoutes(Component)).toEqual(Component);
    expect(withRouter).toHaveBeenCalledWith(Component);
    expect(withContextRoutes).toHaveBeenCalledWith(Component);
    expect(withReactRouterBridge).toHaveBeenCalledWith(Component);
  });
});
