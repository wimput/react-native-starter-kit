import React from 'react';
import RouteContext from 'src/contexts/routes';

export default Component => props => (
  <RouteContext.Consumer>
    {routes => (
      <Component
        {...props}
        routes={routes}
      />
    )}
  </RouteContext.Consumer>
);
