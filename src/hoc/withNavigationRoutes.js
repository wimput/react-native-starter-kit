import { compose } from 'recompose';
import { withRouter } from 'react-router-native';
import withContextRoutes from 'src/hoc/withContextRoutes';
import withReactRouterBridge from 'src/hoc/withReactRouterBridge';

export default compose(withContextRoutes, withRouter, withReactRouterBridge);
