import React from 'react';
import PropTypes from 'prop-types';
import ViewContext from 'src/contexts/views';

export default (Component) => {
  const withRouteComponents = props => (
    <ViewContext.Consumer>
      {views => (
        <Component
          {...props}
          routes={props.routes.map(route => ({
            ...route,
            view: {
              ...route.view,
              component: views[route.view.type]
            }
          }))}
        />
      )}
    </ViewContext.Consumer>
  );
  withRouteComponents.propTypes = {
    routes: PropTypes.array.isRequired
  };
  return withRouteComponents;
};
