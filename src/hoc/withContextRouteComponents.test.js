import withContextRouteCompo from 'src/hoc/withContextRouteComponents';
import withContextRoutes from 'src/hoc/withContextRoutes';
import withRouteComponents from 'src/hoc/withRouteComponents';

jest.mock('src/hoc/withContextRoutes', () => jest.fn().mockImplementation(comp => comp));
jest.mock('src/hoc/withRouteComponents', () => jest.fn().mockImplementation(comp => comp));

const Component = () => {};

describe('withContextRouteComponents', () => {
  it('should combine withContextRoutes and withRouteComponents', () => {
    expect(withContextRouteCompo(Component)).toEqual(Component);
    expect(withContextRoutes).toHaveBeenCalledWith(Component);
    expect(withRouteComponents).toHaveBeenCalledWith(Component);
  });
});
