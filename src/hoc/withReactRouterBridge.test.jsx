import React from 'react';
import { shallow } from 'enzyme';
import withReactRouterBridge from 'src/hoc/withReactRouterBridge';

const routes = [
  {
    id: 'testPage1',
    label: 'TestPage1',
    icon: 'person',
    path: '/path1'
  },
  {
    id: 'testPage2',
    label: 'TestPage2',
    path: '/path2'
  },
  {
    id: 'testPage3',
    label: 'TestPage3',
    icon: 'market',
    path: '/path3'
  }
];

const push = jest.fn();

const Dummy = () => {};

const Test = withReactRouterBridge(Dummy);

const wrapper = shallow(<Test routes={routes} location={{ pathname: '/path2' }} history={{ push }} />);

const enhanceddRoutes = wrapper.find(Dummy).at(0).props().routes;

describe('withReactRouterBridge', () => {
  it('should provide the correct isActive states', () => {
    expect(enhanceddRoutes[0].isActive).toEqual(false);
    expect(enhanceddRoutes[1].isActive).toEqual(true);
    expect(enhanceddRoutes[2].isActive).toEqual(false);
  });

  it('should mask the history push with a navigateTo', () => {
    enhanceddRoutes[1].navigateTo();
    expect(push).toHaveBeenCalledWith('/path2');
  });
});
