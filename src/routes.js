export default [
  {
    id: 'members',
    path: '/members',
    label: 'Members',
    icon: 'person',
    view: {
      type: 'defaultView'
    }
  },
  {
    id: 'classes',
    path: '/classes',
    label: 'Classes',
    icon: 'calendar',
    view: {
      type: 'defaultViewWithTabs'
    },
    routes: [
      {
        id: 'classesList',
        path: '/classes/list',
        label: 'List',
        view: {
          type: 'defaultView'
        }
      },
      {
        id: 'classesCalendar',
        path: '/classes/calendar',
        label: 'Calendar',
        view: {
          type: 'defaultView'
        }
      }
    ]
  },
  {
    id: 'exercises',
    path: '/exercises',
    label: 'Exercises',
    icon: 'book',
    view: {
      type: 'defaultViewWithTabs'
    },
    routes: [
      {
        id: 'exercisesPool',
        path: '/exercises/pool',
        label: 'Pool',
        view: {
          type: 'defaultView'
        }
      },
      {
        id: 'exercisesTags',
        path: '/exercises/tags',
        label: 'Tags',
        view: {
          type: 'defaultView'
        }
      }
    ]
  },
  {
    id: 'coach',
    path: '/coach',
    label: 'Coach',
    icon: 'clipboard',
    view: {
      type: 'defaultView'
    }
  }
];
