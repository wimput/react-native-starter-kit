import { combineReducers } from 'redux';
import members from 'src/store/reducers/members';

const rootReducer = combineReducers({
  members
});

export default rootReducer;
