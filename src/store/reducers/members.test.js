import members from 'src/store/reducers/members';

describe('members', () => {
  it('should return the default state by default', () => {
    expect(members(undefined, { type: false })).toEqual([]);
  });
});
