import React from 'react';
import { NativeRouter } from 'react-router-native';
import { Container, Header, Content, Footer, Body, Title, Left, Right } from 'native-base';
import RouterView from './src/components/navigation/router-view';
import NavBar from './src/components/navigation/nav-bar';
import CombinedProviders from './src/components/combined-providers';

export default () => (
  <CombinedProviders>
    <NativeRouter>
      <Container>
        <Header hasTabs>
          <Left />
          <Body>
            <Title>First Class Coach</Title>
          </Body>
          <Right />
        </Header>
        <Content>
          <RouterView />
        </Content>
        <Footer>
          <NavBar />
        </Footer>
      </Container>
    </NativeRouter>
  </CombinedProviders>
);
